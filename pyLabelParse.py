import csv
import time # to pause after running so people can read the message

name = input("Please enter the name of the Archon-formatted CSV file you wish to make labels for (name only, no extension!): ")

first = True # these two boolean variables are to skip the first and second lines in the file, which, under the Archon format, do not contain useful information.
second = True
currBox = 0
prevBox = 0
firstFolder = "foo" # we need to keep track of the first folder in each box so we can write all the data for a given box into a line in the csv all at once.
currFolder = "foo"
prevFolder = "foo"
ended = False

while True:
	try:
		with open(name + ".csv") as csvFile:
			with open(name + "Labels.csv", 'w', newline = '') as outputFile:
				reader = csv.reader(csvFile, delimiter=',')
				outputter = csv.writer(outputFile, delimiter = ',', quotechar = '"', quoting=csv.QUOTE_MINIMAL)
				outputter.writerow(['Box Number', 'First Folder', 'Last Folder'])
				for row in reader: # row is list type
					if first:
						first = False
						continue
					if second:
						second = False
						continue
					prevFolder = currFolder
					currFolder = row[6]
					if currBox != row[3]:
						if row[3] == "":
							outputter.writerow([str(currBox), firstFolder, prevFolder])
							ended = True
							break
						prevBox = currBox
						currBox = row[3]
						if firstFolder == "foo":
							firstFolder = currFolder
							continue
						outputter.writerow([str(prevBox), firstFolder, prevFolder])
						firstFolder = currFolder
					print(row)
				if (int(prevBox) + 1) is (int(currBox)) and not ended:
					outputter.writerow([str(currBox), firstFolder, currFolder])
				break
	except FileNotFoundError:
		name = input("Input file not found! Please re-enter the file name: ")
print(str(currBox) + " boxes found. Contents have been parsed and each box's first and last folders have been outputted to " + name + "Labels.csv. Thanks for using pyLabelParse!")
time.sleep(8)