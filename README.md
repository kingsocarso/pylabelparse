## Introducing pyLabelParse

pyLabelParse automates the creation of archival box labels from Archon finding aids. It takes in an Archon-formatted finding aid in the CSV format and outputs a CSV which contains three columns: Box Number, First Folder (which contains the first folder in each box), and Last Folder (which contains the last folder in each box).

## Getting and Installing pyLabelParse

**For Windows machines**, simply download the executable within the "dist" directory and put it in the same directory as your desired input Archon-formatted finding aid. Note that the version number is stated in the title of each exe file within dist.

**For all other operating systems**, installation is still pretty simple. Ensure that Python 3 is installed and download pyLabelParse.py. Put it in the same directory as your desired Archon-formatted input finding aid.

## Testing pyLabelParse

An example input CSV file (example.csv) has been provided, as well as what pyLabelParse outputs from it, exampleLabels.csv (pyLabelParse always outputs a CSV file with the input's name and "Label" appended to it). The example file is in fact a portion of the finding aid for the *Sun-Times Movie Press Kits* collection (26/20/119) at the University of Illinois Archives.

To see how pyLabelParse generated exampleLabels.csv and the procedure by which pyLabelParse is used, delete exampleLabels.csv and run the program (see section below on how to run). The prompts and verbose (at the moment) output should be pretty informative.

## Using pyLabelParse

**All users should make sure that their input file is in the CSV format!**

**For Windows machines**, just run the exe to run pyLabelParse and follow the onscreen prompts.

**For all other operating systems**, use your favorite command line interface to navigate to the directory where everything is. Run `python ./pyLabelParse.py` (in some systems this should be `python3 ./pyLabelParse.py`). If Python is not added to PATH, run pyLabelParse.py with whatever Python environment you use. Follow the onscreen prompts.

## Reporting Bugs

Please report all bugs and feature requests to [the repository](https://gitlab.com/kingsocarso/pylabelparse/issues).